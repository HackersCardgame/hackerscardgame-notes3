//////////////////////////////////////////////////////
// Place for shared elements
//////////////////////////////////////////////////////
(function() {
    var logger = {
        info: function() {
            logger._log('info', Array.prototype.slice.call(arguments));
        },
        warn: function() {
            logger._log('warning', Array.prototype.slice.call(arguments));
        },
        error: function() {
            logger._log('error', Array.prototype.slice.call(arguments));
        },
        _log: function __log(type, args) {
            var message = args.shift();

            if (typeof Raven !== 'undefined') {
                Raven.captureMessage(message, {
                    level: type,
                    extra: args
                });
            }
        }
    };

    window.logger = logger;

  angular
    .module('creditCard', []);

  //directive which adds spaces to user input through parserFunction
  //and detects credit card type
  angular
    .module('creditCard')
    .directive('creditCardInput', CreditCardInput);

  function CreditCardInput(cc) {
    return {
      require: 'ngModel',
      link: function(scope, element, attrs, ctrl) {
        var el = element[0];
        var divider = ' ';

        function noSpaces(value) {
          if (value) {
            return value.replace(/ /g, '');
          }
        }

        function formatInput(value) {
          return value.match(/.{1,4}/g).join(divider);
        }

        //runs when input field is updated
        function parserFunction(value) {
          if (value) {
            var noSpace = noSpaces(value);
            var spaced = formatInput(noSpace); //first clear spaces and then add - prevent spaces duplication

            // Avoid infinite loop of $setViewValue <-> $parsers
            if (value === spaced) return noSpace;

            //how many dividers WERE in input - BEFORE our input last element
            var dividersBefore = (value.slice(0, el.selectionStart).split(divider)).length - 1;
            //how many dividers ADDED to the input - BEFORE our input last element
            var dividersAfter = (spaced.slice(0, el.selectionStart + 1).split(divider)).length - 1; //will always return array of at least 1
            var dividerDiff = dividersAfter - dividersBefore;

            var pos = el.selectionStart + dividerDiff; //selection start

            ctrl.$setViewValue(spaced);
            ctrl.$render();

            el.setSelectionRange(pos, pos); //set coursor back to it's position- avoid jumping to the end of input
            return noSpace;
          }
        }

        function cardType(number) {
          cc.type = '';
          if (!number) {
            return;
          }
          var cards = {
            //electron: /^(4026|417500|4405|4508|4844|4913|4917)\d+$/,
            //maestro: /^(5018|5020|5038|5612|5893|6304|6759|6761|6762|6763|0604|6390)\d+$/,
            //dankort: /^(5019)\d+$/,
            //interpayment: /^(636)\d+$/,
            //unionpay: /^(62|88)\d+$/,
            visa: /^4[0-9]{6,}$/, //Visa card numbers start with a 4.
            //MasterCard numbers start with the numbers 51 through 55,
            //Or MasterCard numbers start with 2221 through 2720
            // ((2(2[1-9]|[3-9][0-9]))  - for 221-229  and  230-299
            // (([3-6][0-9][0-9]))      - for 300-699
            // (7(([01][0-9])|(20))))   - for 700-720
            mastercard: /(^5[1-5][0-9]{5,}$)|(^2((2(2[1-9]|[3-9][0-9]))|(([3-6][0-9][0-9]))|(7(([01][0-9])|(20))))[0-9]{4,})/,
            amex: /^3[47][0-9]{5,}$/, //American Express card numbers start with 34 or 37.
            diners: /^3(?:0[0-5]|[68][0-9])[0-9]{4,}$/, // begin with 300 through 305, 36 or 38.
            discover: /^6(?:011|5[0-9]{2})[0-9]{3,}$/, //Discover card numbers begin with 6011 or 65.
            unionpay: /^62[468][0-9][0-9]{3,}$/, //UnionPay card numbers begin with 624, 626 or 628.
            jcb: /^(?:2131|1800|35[0-9]{3})[0-9]{3,}$/ //JCB cards begin with 2131, 1800 or 35.
          };
          for (var card in cards) {
            if (cards.hasOwnProperty(card)) {
              if (cards[card].test(number)) {
                cc.type = card;
              }
            }
          }
          return number;
        }

        // 16 characters
        attrs.$set('maxlength', 24); //19 digits + 19/4= 4 spaces

        ctrl.$parsers.push(parserFunction);

        if (attrs.creditCardType) {
          ctrl.$parsers.push(cardType);
        }

      }
    };
  }

  //directive which sets the value of $scope.ccType to credit card Type
  //usage: ng-class="ccType". Please also add 'credit-card-type' class if ccType is true
  angular.module('creditCard').directive('creditCardTypeClass', CreditCardTypeClass);

  function CreditCardTypeClass(cc) {
    return {
      link: function(scope) {
        scope.$watch(
          function() {
            return cc.type;
          },
          function(newVal) {
            scope.ccType = cc.type;
          });
      }
    };
  }

  //allows to access cc.type in any directive
  angular
    .module('creditCard')
    .value('cc', {type: ''});

  /**
   * TNP-2325 - Add terms of service and privacy policy links to both themes
   */
  angular.module('app').directive('termsOfService', function(env) {
    return {
      restrict: 'E',
      scope: false,
      link: function postLink(scope, iElement, iAttrs) {
        var TERMS_OF_SALE_US = 'https://www.fastspring.com/terms-sale-us.html?utm_source=Store&utm_medium=Terms_of_Sale_US&utm_content=Foundation&utm_campaign=Store_Traffic';
        var TERMS_OF_SALE_EU = 'https://www.fastspring.com/terms-sale-eu.html?utm_source=Store&utm_medium=Terms_of_Sale_EU&utm_content=Foundation&utm_campaign=Store_Traffic';
        var PRIVACY_POLICY = 'https://fastspring.com/privacy?utm_source=Store&utm_medium=Privacy_Policy&utm_content=Foundation&utm_campaign=Store_Traffic';

        var euCountries = ['AT', 'BE', 'BG', 'HR', 'CY', 'CZ', 'DK', 'EE', 'FI', 'FR', 'DE', 'GR', 'HU', 'IE', 'IT', 'LV', 'LT', 'LU', 'MT', 'NL', 'PL', 'PT', 'RO', 'SK', 'SI', 'ES', 'SE', 'GB'];

        var policies = {
          DE: {terms: 'https://fastspring.com/fastspring-terms-sale-germany/', privacy: 'https://fastspring.com/fastspring-privacy-policy-germany/'},
          JP: {terms: 'https://fastspring.com/terms-use/terms-sale-japan/', privacy: 'https://fastspring.com/privacy/fastspring-privacy-policy-japan/'},
          FR: {terms: 'https://fastspring.com/terms-use/fastspring-terms-sale-france/', privacy: 'https://fastspring.com/privacy/fastspring-privacy-policy-france/'}
        };

        scope.$watchCollection(function() { return [scope.country, scope.language] }, function(newValue, oldValue) {
          if (policies[scope.country] && policies[scope.country].terms) {
            scope.complianceData.termsURL = policies[scope.country].terms;
          } else {
            scope.complianceData.termsURL = euCountries.indexOf(scope.country) > -1 ? TERMS_OF_SALE_EU : TERMS_OF_SALE_US;
          }

          if (policies[scope.country] && policies[scope.country].privacy) {
            scope.complianceData.privacyURL = policies[scope.country].privacy;
          } else {
            scope.complianceData.privacyURL = PRIVACY_POLICY;
          }

          if (scope.complianceData.enabled) {
            scope.complianceData.template = env.phrases['AgreementTermsAndPolicy'];
          }
        });

      },
      template: "<a href ng-href=\"{{ complianceData.privacyURL }}\" target=\"_blank\" rel=\"noopener noreferrer\">{{ 'PrivacyPolicy' | phrase }}</a>&nbsp;|&nbsp;<a href ng-href=\"{{ complianceData.termsURL }}\" target=\"_blank\" rel=\"noopener noreferrer\">{{ 'TermsOfSale' | phrase }}</a><span ng-if=\" country == 'RU' \">&nbsp;|&nbsp;<a href ng-href=\"https://fastspring.com/terms-use/license-agreement/\" target=\"_blank\" rel=\"noopener noreferrer\">{{'LicenseAgreement' | phrase}}</a></span>"
    }
  });

  angular.module('app').directive('compile', function($compile) {
    return function(scope, elem, attr) {
      scope.$watch(function(scope) {
        return scope.$eval(attr.compile);
      }, function(val) {
        elem.html(val);
        $compile(elem.contents())(scope);
      });
    }
  });

  //Keeping all helper functions in helper service
  angular.module('app').factory('helpers', function($filter){
    // originally from subscription factory
    //this function pluralizes given unit in english and returns its localized value from messages file
    function pluralize(length, unit) {
      if (length == 1) {
        return $filter('phrase')(unit);
      } else if (length > 1) {
        return $filter('phrase')(unit + 's');
      }
    }

    function germanJede(length, unit, flag) {
      if (length > 1) {
          return flag ? 'Alle' : 'alle';
      } else if (unit === 'week') {
          return flag ? 'Jede' : 'jede';
      } else if (unit === 'month' || unit === 'day') {
          return flag ? 'Jeden' : 'jeden';
      } else if (unit === 'year') {
          return flag ? 'Jedes' : 'jedes';
      }
    }

    //Todo: the following functions should be a part of this service
    //function autorenewOffWarning(item) {
    //function onAutoRenewChange(item) {

    function isToday(value, displayValue) {
      var dateReceived = new Date(value);
      if (dateReceived.toDateString() === (new Date()).toDateString() || !value) {
        return $filter('phrase')('Today');
      } else {
        return displayValue;
      }
    }
    return {
      pluralize: pluralize,
      isToday: isToday,
      germanJede: germanJede
    }
  });

  // Filter which returns string value set in MANIFEST file
  angular.module('app').filter('phrase', ['$filter', 'env', function($filter, env) {
    function formatString(phrase, inputs){
      for (var i = 1; i < inputs.length; i++) {
        if (env.phrases[inputs[i]]) {
          inputs[i] = env.phrases[inputs[i]];
        }

        phrase = phrase.replace("%s", inputs[i] || '');
      }
      return phrase;
    }
    function doTranslation(v) {
      if (v) {
        var V = v.charAt(0).toUpperCase() + v.slice(1).toLowerCase() || v;

        if (arguments.length > 1) {
          var formattedString;

          if (v in env.phrases) {
            formattedString = formatString(env.phrases[v], arguments);
          } else if (V in env.phrases) {
            formattedString = formatString(env.phrases[V], arguments);
          }

          return formattedString;
        } else {
          return env.phrases[v] || env.phrases[V] || v;
        }
      }
    };
    doTranslation.$stateful = true;
    return doTranslation;
  }]);

  angular.module('app').service('applePay', ApplePayProvider);

  ApplePayProvider.$inject = ['$filter', '$http', '$rootScope', 'taxUpdater'];

  function ApplePayProvider($filter, $http, $rootScope, taxUpdater) {
    var that = this;
    var merchantId = 'merchant.com.fastspringlive.prod';
    if (/localhost/.test(window.location.hostname) || /\.qa\./.test(window.location.href)) {
      merchantId = 'merchant.com.fastspring.prelive';
    }
    this.init = function _init(endpoint, callback) {
      if (typeof endpoint === 'undefined' || !endpoint) {
        throw new Error('No ApplePay validation service endpoint provided');
      }
      if (window.ApplePaySession) {
        var promise = ApplePaySession.canMakePaymentsWithActiveCard(merchantId);
        promise.then(function _canMakePayments(canMakePayments) {
          that._canMakePayments = canMakePayments;
          if (typeof callback === 'function') {
            callback(canMakePayments);
          }
        });
        that.endpoint = endpoint;
      }
    };

    this.getTotal = function _getTotal(scope) {
      return {
        label: $filter('phrase')('ViaFastspring'),
        amount: scope.totalWithTaxValue
      };
    };

    this.getLineItems = function _getLineItems(scope) {
      var orderGroups = scope.groups || scope.order.groups;
      function getItems(groups) {
        var items = [];
        if (groups && groups.length > 0) {
          groups.forEach(function(group) {
            if (group.selections) {
              group.items.forEach(function(item) {
                if (item.selected) {
                  items.push(item);
                  items.concat(getItems(item.groups));
                }
              });
            }
          });
        }
        return items;
      }

      var cart = getItems(orderGroups);

      if (cart.length === 0) {
        throw new Error('Cart is empty, cannot proceed with payment!');
      }

      var lineItems = [];
      cart.forEach(function (item) {
        lineItems.push({
          label: item.display || item.path,
          amount: item.totalValue
        });
      });

      if (scope.taxValue > 0) {
        lineItems.push({
          label: $filter('phrase')('Tax'),
          amount: scope.taxValue
        });
      }

      return lineItems;
    };

    this.doPayment = function _doPayment(paymentRequest, subscribe) {
      if (typeof this._canMakePayments === 'undefined') {
        throw new Error('ApplePayProvider has not been initialized!');
      }
      var session = new ApplePaySession(1, paymentRequest);
      session.onvalidatemerchant = function(event) {
        validateApplePay(event).then(function (result) {
          session.completeMerchantValidation(result.data);
        }, function (err) {
          console.error('Failure validating ApplePay!', err);
        });
      };

      session.onshippingcontactselected = function(event) {
        var contact = {
          email: event.shippingContact.emailAddress,
          phoneNumber: event.shippingContact.phoneNumber,
          region: event.shippingContact.administrativeArea,
          country: event.shippingContact.countryCode,
          city: event.shippingContact.locality,
          postalCode: event.shippingContact.postalCode,
          firstName: event.shippingContact.givenName,
          lastName: event.shippingContact.familyName,
          addressLine1: event.shippingContact.addressLines && event.shippingContact.addressLines[0],
          addressLine2: event.shippingContact.addressLines && event.shippingContact.addressLines.length > 1 ?
            event.shippingContact.addressLines[1] : undefined
        };

        $rootScope.$broadcast('new-contact', contact);
        taxUpdater.update(contact.postalCode).then(function _s(result) {
          var cancel = $rootScope.$on('viewdata-updated', function(event, scope) {
            var newLineItems = that.getLineItems(result.data.order);
            var newTotal = that.getTotal(result.data.order);
            session.completeShippingContactSelection(ApplePaySession.STATUS_SUCCESS, [], newTotal, newLineItems);
            cancel();
          });
          $rootScope.$broadcast('new-viewdata', result.data);
        }, function _e(reason) {
          console.error('Failure gathering contact information', reason);
          $rootScope.$broadcast('add-alert', 'ErrorServer');
          session.abort();
        });
      };

      session.onpaymentauthorized = function(event) {
        var billingContact = event.payment.billingContact;
        var shippingContact = event.payment.shippingContact;
        var data = {
          paymentType: 'applepay',
          subscribe: subscribe,
          applepay: event.payment.token
        };

        if (billingContact || shippingContact) {
          data.contact = {};
          angular.extend(shippingContact || {}, billingContact || {});
          data.contact.firstName = shippingContact.givenName;
          data.contact.lastName = shippingContact.familyName;
          data.contact.region = shippingContact.administrativeArea;
          data.contact.country = shippingContact.countryCode;
          data.contact.city = shippingContact.locality;
          data.contact.postalCode = shippingContact.postalCode;
          data.contact.email = shippingContact.emailAddress;
          data.contact.phoneNumber = shippingContact.phoneNumber;

          if (shippingContact.addressLines) {
            data.contact.addressLine1 = shippingContact.addressLines[0];
            data.contact.addressLine2 = shippingContact.addressLines.length > 1 ? shippingContact.addressLines[1] : undefined;
          }
        }

        sendPayment(data);
      };

      session.oncancel = function(event) {
        logger.info('ApplePay oncancel');
        $rootScope.$broadcast('setPaymentOptions');
      };

      // Unfortunately we have this recreate this function
      function sendPayment(data) {
        $http({method: 'POST', url: '/payment', data: data})
          .then(function successfulPayment(response) {
            $rootScope.$broadcast('GAsendAll', 'ApplePay');
            var result = response.data;
            if (result.next == 'error') {
              analyze({
                '_private': true,
                'fsc-exception': {
                  'exDescription': 'Payment Error: ' + (result.messages && JSON.stringify(result.messages)) + (result.validation && JSON.stringify(result.validation)),
                  'exFatal': true,
                  'appName': 'Payment Flow'
                },
                'fsc-paymentMethod': data.paymentType
              });
              $rootScope.$broadcast('add-alert', 'ErrorServer');
              session.abort();
            } else if (result.next === 'redirect') {
              session.completePayment(ApplePaySession.STATUS_SUCCESS);
              location.href = result.url;
            } else {
              $rootScope.$broadcast('add-alert', 'ErrorServer');
              session.abort();
            }
          },
          function failedPayment(reason) {
            debug('Failed submitting payment', reason);
            $rootScope.$broadcast('add-alert', 'ErrorServer');
          });
      }
      session.begin();
    };

    function validateApplePay(session) {
      return $http({
        method: 'POST',
        url: that.endpoint + '/validate',
        data: {
          validationURL: session.validationURL,
          merchantID: merchantId,
          domain: window.location.host,
          displayName: (document.title && document.title.trim()) || $filter('phrase')('FastspringCheckout')
        }
      });
    }
  }

  angular.module('app').directive('applePayButton', ApplePayButton);

  function ApplePayButton() {
    return {
      restrict: 'E',
      scope: false,
      template: '<div class="apple-pay-button apple-pay-button-black" ng-click="doApplePay()"></div>',
      controller: ['$scope', '$filter', '$rootScope', 'applePay', 'env', function _ApplePayButtonController($scope, $filter, $rootScope, applePay, env) {
        applePay.init($scope.services['applepay.service.endpoint'], function(canMakePayments) {
          function setApplePay(val, digest) {
            function setter(scope) {
              if (val) {
                scope.applePaySupported = canMakePayments;
                scope.showOtherPaymentMethods = !canMakePayments;
                scope.paymentOptionType = 'applepay';
              } else {
                scope.applePaySupported = false;
                scope.showOtherPaymentMethods = true;
                scope.applePayAllowed = false;
              }

              if ($scope.applePayAllowed && $scope.applePaySupported) {
                $rootScope.showContact = false;
              } else {
                $rootScope.showContact = !env.hasAccount;
              }
            }
            if (digest) {
              $scope.$apply(setter);
            } else {
              setter($scope);
            }
          }

          $scope.$watch(function() { return $scope.applePayAllowed; }, function (allowed) {
            setApplePay(allowed, false);
          });
          setApplePay($scope.applePayAllowed, true);

          $scope.showAllPaymentMethods = function _showAllPaymentMethods() {
            $scope.paymentOptionType = undefined;
            $scope.setPaymentOptions();
            $scope.showOtherPaymentMethods = true;
          };
        });

        $scope.doApplePay = function() {
          $scope.paymentOptions.forEach(function(option) {
            if (option.type === 'applepay') {
              $scope.option = option;
              $scope.paymentOptionType = option.type;
            }
          });

          if (!$scope.option || $scope.paymentOptionType !== 'applepay') {
            throw new Error('Apple Pay checkout not allowed!');
          }

          var paymentRequest = {
            countryCode: $scope.country,
            currencyCode: $scope.currency,
            supportedNetworks: ['amex', 'discover', 'visa', 'masterCard'],
            merchantCapabilities: ['supportsCredit', 'supportsDebit', 'supports3DS'],
            lineItems: applePay.getLineItems($scope),
            total: applePay.getTotal($scope),
            requiredBillingContactFields: [],
            requiredShippingContactFields: []
          };

          if ($scope.option.requireContact) {
            paymentRequest.requiredShippingContactFields.push('name');
            paymentRequest.requiredShippingContactFields.push('email');
          }

          if ($scope.option.requireShipping || $scope.variables.forcePhysicalAddressCollection === 'true'
            || ($scope.option.requireBillingPostal && !$scope.validZipCode)) {
            paymentRequest.requiredShippingContactFields.push('postalAddress');
          }

          if ($scope.variables.forcePhoneNumberCollection === 'true') {
            paymentRequest.requiredShippingContactFields.push('phone');
          }

          // Set icon for payment sheet
          var link = document.querySelector('link[rel="apple-touch-icon"]');
          var product = $scope.getFirstSelectedProductWithImage();
          if (link && product && product.image) {
            link.setAttribute('href', product.image);
          }

          applePay.doPayment(paymentRequest, $scope.mailingList && $scope.mailingList.subscribe);
        };
      }]
    }
  }

  angular.module('app').service('taxUpdater', TaxUpdaterService);

  TaxUpdaterService.$inject = ['$http', '$q'];

  function TaxUpdaterService($http, $q) {
    var updating = false;

    this.update = function _update(postalCode) {
      var deferred = $q.defer();
      if (updating) {
        deferred.reject('Already updating!');
      } else {
        $http({
          method: 'POST', url: '/tax', data: {
            postalCode: postalCode
          }
        }).then(function success(result) {
          updating = false;
          deferred.resolve(result);
        }, function fail(reason) {
          updating = false;
          deferred.reject(reason);
        });
      }
      return deferred.promise;
    }
  }

  angular.module('app').factory('httpInterceptor', HttpInterceptor);

  HttpInterceptor.$inject = ['env', '$rootScope', '$q'];

  function HttpInterceptor(env, $rootScope, $q) {

    return {
      request: function(config) {
        if (!/^http/.test(config.url) && config.url.indexOf('.html') < 0) {
          if (!/\/background$/.test(config.url)) {
            $rootScope.$broadcast('LoadStart');
          } else {
            config.url = config.url.replace(/\/background$/, '');
          }
          config.url = env.lastSessionUrl + config.url;
          config.headers['X-Session-Token'] = env.lastSessionToken;
        }
        return config;
      },
      response: function(response) {
        if (response.config.url.indexOf('.html') < 0) {
          $rootScope.$broadcast('LoadStop');
          var url = response.headers['X-Session-Location'];
          var token = response.headers['X-Session-Token'];
          if (url) {
            env.lastSessionUrl = url;
          }

          if (token) {
            env.lastSessionToken = token;
          }
        }
        return response;
      },
      requestError: function(response) {
        $rootScope.$broadcast('LoadStop');
        return $q.reject(response);
      },
      responseError: function(response) {
        var result;
        if (response && response.config) {
          if (response.config.url.indexOf('.html') < 0) {
            result = response.data && response.data.toString();
          }
        } else {
          result = response;
        }

        if (result == 'token-mismatch') {
          $rootScope.showWaitDialog('WaitRedirectTitle', 'WaitRedirectAbout');
          location.reload(true);
        } else if (result !== 'invalid-country') {
          var phrase = 'Unexpected';
          var type = 'danger';

          if (result === 'multiple-variations-of-the-same-product-cannot-be-added') {
            phrase = 'MultipleVariations';
            type = 'warning';
          }

          $rootScope.messages = [{
            type: type,
            phrase: phrase
          }];
        }
        $rootScope.$broadcast('LoadStop');
        return $q.reject(response);
      }
    }
  }

  angular.module('app').config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('httpInterceptor');
  }]);

})();
