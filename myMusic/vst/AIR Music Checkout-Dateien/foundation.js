(function() {
  'use strict';

// Auto-populated
  var _variables = {"integrationBehaviourGA":"default","collectingEmailCheckbox":"true","cartCrossSellsPositionProduct":"inside","purchaseBtn":"addToCart","confirmCancelBtnFontSize":"14px","showCouponField":"paymentBlock","productImageSize":"large","crossSellsPositionProduct":"inside","nortonDisplay":"large","upSellsDisplay":"pageLightbox","popupHeadingAlign":"center","showCompanyField":"disable","offerTitleSize":"16px","subscriptionAutoRenew":"auto","showVatLink":"true","topbarFreezing":"false","listUpSellsDisplay":"page","upSellsPosition":"belowdriver","cartUpSellsDisplay":"pageLightbox","panelHeaderTextColor":"#333333","countrySelector":"visible","offerImageSizeInsideCart":"medium","listUpSellsPosition":"belowdriver","popupOnWindowClose":"true","integrationBehaviourGTM":"default","productTitle":"showDescription","licenses":"true","cartUpSellsPosition":"belowdriver","requireEmail":"false","panelHeaderColorNew":"#F5F5F5","font":"Montserrat","offerImageSizeOutsideCart":"large","listCrossSellsPositionProduct":"belowdriver","logo":"https://d8y8nchqlnmka.cloudfront.net/M4tJVbQ6Szk/oTH0QsiVSmA/air_logo2012WH.png","subscriptionUsePaymentMethodBtnFontSize":"14px","priceSize":"large","showListCheckbox":"false","textAlign":"left","productTitleSize":"18px","managePaymentMethodBtnFontSize":"14px","popupTextAlign":"center","forcePhoneNumberCollection":"false","couponFieldExpanded":"true","shortCheckout":"true","savePaymentDetailsBtnFontSize":"14px","popupOnTimeout":"true","addPaymentMethodBtnFontSize":"12px","forcePhysicalAddressCollection":"true","popupButtonAlign":"center","confirmOkBtnFontSize":"14px","customIdGTM":"","addToCartBtnFontSize":"14px","crossSellsPositionStorefront":"inside","logoRetina":"https://d8y8nchqlnmka.cloudfront.net/M4tJVbQ6Szk/Llp2KJj5QyU/air_logo2012WH.png","showEmailOnCartPage":"payment","popupImage":"https://d8y8nchqlnmka.cloudfront.net/M4tJVbQ6Szk/WXKDrzv8R58/save10Popup.jpg","customIdGA":"","hideFree":"false","manageSubscriptionBtnFontSize":"14px"};

  /*
   * Error tracking
   */
  if (typeof Raven !== 'undefined') {
    try {
      var sinfo = window.location.hostname.match(/([^.]+)/);
      var path = 'default';
      if (sinfo) {
        var store = sinfo.pop();
        if (window.location.pathname.length > 3) {
          var pmatch = window.location.pathname.match(/\/([^/]+)\/?/);
          if (pmatch) {
            path = pmatch.pop();
          }
        }
      }
      var DSN = 'https://fae433c938b2406583feaf00d3105493@logger.fastspring.com/5';
      if (/\.qa\./.test(window.location.hostname)) {
        DSN = 'https://ad563db008d44020bb3735c8b791bcb1@qa-logger.fastspring.com/4';
      }
      Raven.config(DSN, {
        dataCallback: function(data) {
          if (data && data.exception && data.exception.values && data.exception.values.length > 0 && data.exception.values[0].type) {
            var fsRegex = /([^\s]*.onfastspring.com[^\s]*)/g;
            var exceptionValue = fsRegex.test(data.exception.values[0].value) ? data.exception.values[0].value.replace(fsRegex, "'exceptionUrl'") : data.exception.values[0].value;
            data.fingerprint = [data.exception.values[0].type, exceptionValue];
          }
          return data;
        },
        release: document.querySelector('#viewBranch') && document.querySelector('#viewBranch').getAttribute('content')
      }).install();
      Raven.setTagsContext({
        'sandbox': false,
        'session-id': viewDataFactory().session.token,
        'path': path,
        'store': store
      });
    } catch (ignore) {}
  }

  function startUpdate() {
    angular.element(document.getElementById('app')).addClass('updating');
  }
  function endUpdate() {
    angular.element(document.getElementById('app')).removeClass('updating');
  }

  var fsApp = angular.module("app", ['ngSanitize', 'ui.bootstrap', 'app.controllers', 'nsPopover']);

  fsApp.factory('initData', viewDataFactory);
  function viewDataFactory() {
    if (viewDataFactory.data) {
      return viewDataFactory.data;
    }
    var data = document.getElementById('viewdata');
    data = data && angular.fromJson(angular.element(data).text());
    if (!viewDataFactory.data) {
      viewDataFactory.data = data;
    }
    return data;
  }

  fsApp.factory('fulfillments', function() {
    function create(fulfillments) {
      var files = [];
      var licenses = [];
      for (var fulfillmentItem in fulfillments) {
        var fulfillment = fulfillments[fulfillmentItem];
        if (fulfillment instanceof Array) {
          fulfillment.forEach(function(f) {
            if (f.type == 'file') { files.push(f); }
            if (f.type == 'license') { licenses.push(f); }
          });
        }
      };
      return {
        'files' : files,
        'licenses' : licenses
      };
    }

    return {
      create: create
    };
  });


  fsApp.factory('env', function() {
    return {'lastSessionToken': '', 'lastSessionUrl': '', 'phrases': {}};
  });


  fsApp.filter('price', ['$filter', function($filter) {
    return function(num) {
      return (num.replace(/[^0-9.]/g, '') == '0.00') ? $filter('phrase')('Free') : num;
    };
  }]);
  fsApp.filter('priceColor', ['$filter', function($filter) {
    return function(num) {
      return (num.replace(/[^0-9.]/g, '') == '0.00') ? 'text-success' : 'text-danger price-color';
    };

  }]);

//retina-image attribute sets image width to be twice smaller than it's actual.
//Is used to serve retina image for non-retina devices if the size of image is unknown
  fsApp.directive('retinaImage', function() {
    return {
      restrict: 'EA',
      link: function($scope, element, attrs, ctrl) {
        return element.on('load', function() {
          element[0].width = element[0].width / 2;
          element[0].style.opacity = 1;
        });
      }
    };
  });


  fsApp.directive('completeItem', function($filter){
    return {
      restrict: 'AE',
      scope: {item: '=', ordergroup: '=', offer: '=', config: '=', language: '='},
      templateUrl: 'complete-item.html',
      controller: function($scope, fulfillments, helpers) {
        //Item as parameter and item as scope are not necessarily both needed
        $scope.imageStyle = function(item) {
          return $scope.$parent.imageStyle(item);
        };
        $scope.customColor = function(color) {
          return $scope.$parent.customColor(color);
        };
        $scope.addBlankTargets = function(s) {
          return $scope.$parent.addBlankTargets(s);
        };
        $scope.isToday = function(value, displayValue) {
          return helpers.isToday(value, displayValue);
        };
        $scope.pluralize = function (length, unit) {
          return helpers.pluralize(length, unit);
        };
        if ($scope.item.subscription) {
            $scope.germanJede = helpers.germanJede($scope.item.subscription.intervalLength, $scope.item.subscription.intervalUnit, true);
        }
        
        $scope.$http = $scope.$parent.$http;
        $scope.variables = $scope.$parent.variables;

        //complete page only: build files and licenses for every item
        $scope.files = fulfillments.create($scope.item.fulfillments).files;
        $scope.licenses = fulfillments.create($scope.item.fulfillments).licenses;

        $scope.showTerms = /^(options|config-(one|many))$/.test($scope.config);
      }
    };
  });

  fsApp.directive('addonItem', function($filter){
    return {
      restrict: 'AE',
      scope: {item: '=', ordergroup: '=', offer: '='},
      templateUrl: 'addon-item.html',
      controller: function($scope, fulfillments, helpers) {
        //Item as parameter and item as scope are not necessarily both needed
        $scope.imageStyle = function(item) {
          return $scope.$parent.imageStyle(item);
        };
        $scope.customColor = function(color) {
          return $scope.$parent.customColor(color);
        };
        $scope.addBlankTargets = function(s) {
          return $scope.$parent.addBlankTargets(s);
        };
        $scope.isToday = function(value, displayValue) {
          return helpers.isToday(value, displayValue);
        };
        $scope.pluralize = function (length, unit) {
          return helpers.pluralize(length, unit);
        };

        $scope.germanJede = function (length, unit) {
            return helpers.germanJede(length, unit);
        };
        $scope.$http = $scope.$parent.$http;
        $scope.variables = $scope.$parent.variables;

        //complete page only: build files and licenses for every item
        $scope.files = fulfillments.create($scope.item.fulfillments).files;
        $scope.licenses = fulfillments.create($scope.item.fulfillments).licenses;
      }
    };
  });

  fsApp.directive('bundleItem', function($filter){
    return {
      restrict: 'AE',
      scope:{ item: '=', ordergroup: '=', offer: '='},
      templateUrl: 'bundle-item.html',
      controller: function($scope, fulfillments) {
        //Item as parameter and item as scope are not necessarily both needed
        $scope.imageStyle = function(item) {
          return $scope.$parent.imageStyle(item);
        }
        $scope.addBlankTargets = function(s) {
          return $scope.$parent.addBlankTargets(s);
        }

        //build files and licenses for every item
        $scope.files = fulfillments.create($scope.item.fulfillments).files;
        $scope.licenses = fulfillments.create($scope.item.fulfillments).licenses;

        $scope.variables = $scope.$parent.variables;
      }
    };
  });


  fsApp.directive('renewsCheckbox', function(helpers, $rootScope) {
    return {
      restrict: 'EA',
      scope: {
        subscription: '=',
        item: '=',
        language: '='
      },
      templateUrl: 'renews-checkbox.html',
      controller: ['$scope', 'initData', function($scope, initData) {
        $scope.subscriptionType = {
          adhoc: !!($scope.subscription.intervalUnit == 'adhoc'),
          hasTrial: !!($scope.subscription.instructions && $scope.subscription.instructions[0] &&
          $scope.subscription.instructions[0].type == 'trial'),
          finiteEndDate: $scope.subscription.instructions[$scope.subscription.instructions.length - 1].periodEndDate,
          free: !$scope.subscription.nextChargeDate && $scope.subscription.nextChargeTotalValue == 0
        };

        $scope.http = $scope.$parent.$http;
        $scope.refreshOrderResponse = $scope.$parent.refreshOrderResponse;
        $scope.pluralize = helpers.pluralize;
        $scope.savedPaymentInfo = initData.autoRenew;

        $scope.germanJede = helpers.germanJede;
        $scope.subscription.germanJede = $scope.germanJede($scope.subscription.intervalLength, $scope.subscription.intervalUnit);
        $scope.subscription.intervalUnitPluralized = $scope.pluralize($scope.subscription.intervalLength, $scope.subscription.intervalUnit);
        $scope.subscription.intervalLengthAdjusted = $scope.subscription.intervalLength > 1 && $scope.subscription.intervalLength;

        if ($scope.subscription && $scope.subscription.instructions && $scope.subscription.instructions[0]) {
          var instructions = $scope.subscription.instructions;
          var instructionInterval = {};

          for (var i = 0; i < instructions.length; i++) {
            if (instructions[i].type !== 'trial' && (instructions[i].intervalUnit !== instructionInterval.unit || instructions[i].intervalLength !== instructionInterval.length)) {
              if (!instructionInterval.unit) {
                instructionInterval = {
                  length: instructions[i].intervalLength,
                  unit: instructions[i].intervalUnit
                };
                continue;
              }

              $scope.subscription.unequalIntervals = true;
              break;
            }
          }
        }

        $scope.isToday = function(value, displayValue) {
          return helpers.isToday(value, displayValue);
        };
        $scope.accountManagement = $rootScope.accountManagement;

      }]
    };
  });


  fsApp.directive('chooseOneMany', function() {
    return {
      restrict: 'AE',
      scope: {item: '=', config: '=', offer: '=', ordergroup: "="},
      templateUrl: 'choose-one-many.html',
      controller: function($scope) {
        $scope.imageStyle = function(item) {
          return $scope.$parent.imageStyle(item);
        };

        $scope.showDescription = function(item) {
          return $scope.$parent.showDescription(item);
        };

        $scope.BtnClass = function(type) {
          return $scope.$parent.BtnClass(type);
        };

        $scope.customColor = function(color) {
          return $scope.$parent.customColor(color);
        };

        $scope.session = $scope.$parent.session;
        $scope.variables = $scope.$parent.variables;
        $scope.nextToDriver = $scope.$parent.nextToDriver;
        $scope.offers = $scope.$parent.offers;

        if ($scope.config == 'one') {
          $scope.type = 'radio';
        }
        else if ($scope.config == 'many') {
          $scope.type = 'checkbox';
        }

      }
    };
  });

  angular.module('app.controllers', []).controller('controller',
    ['$scope', '$http', 'initData', '$modal', 'env', '$filter', '$timeout', '$rootScope',
      function($scope, $http, initData, $modal, env, $filter, $timeout, $rootScope) {
        $scope.http = function(method, path, data, success, fail) {
          startUpdate();
          var config = {
            method: method,
            url: env.lastSessionUrl + path,
            headers: {'X-Session-Token': env.lastSessionToken},
            data: data
          };
          $http(config).
          success(function(result, status, headers, config) {
            env.lastSessionToken = headers('X-Session-Token');
            env.lastSessionUrl = headers('X-Session-Url');
            success(result);
          }).
          error(function(result, status, headers, config) {
            $scope.messages = [{type: 'error', display: 'Unexpected'}];
            if (angular.isDefined(fail)) {
              fail(result);
            }
          })['finally'](endUpdate);
        };

        $scope.addBlankTargets = function(s) {
          return ("" + s).replace(/<a\s+href=/gi, '<a target="_blank" href=');
        };

        //Builds offer arrays
        $scope.createArrays = function() {
          var repeat = true;
          var elem = $scope.groups;
          var driver;
          var type;
          $scope.offers = {
            belowdriver: {},
            below: [],
            above: [],
            underpayment: [],
            inside: [],
            lightbox: [],
            configOne: [],
            configMany: []
          };

          $scope.pushItemBelowDriver = function(driver, group, item) {
            $scope.offers['belowdriver'][driver].push(group);
            if (typeof $scope.offers['belowdriver'][driver][0].items !== 'undefined') {
              var index = $scope.offers['belowdriver'][driver].indexOf(group);
              angular.forEach($scope.offers['belowdriver'][driver][index].items, function(driverItem, key) {
                $scope.offers.belowDriverList.push(driverItem.path);
              });
            }
          };

          $scope.pushItem = function(position, group, item) {
            $scope.offers[position].push(group);
            if (position == 'configOne') {
              group.items.forEach(function(item) {
                if (item.selected == true) {
                  item.replaceSelection = item.path;
                }
              })
            }
          };

          //iterate all items
          $scope.iterate = function(repeat, elem) {
            elem.forEach(function(group) {
              if (group.type != undefined) {
                type = group.type;
              } else {
                type = false;
              }
              if (group.driver != undefined) {
                driver = group.driver;
              } else {
                driver = false;
              }
              $scope.offers.orderArray = $scope.offers.orderArray || [];
              /*TODO: optomise code. Rewrite it!!!
               For now only nexttodriver is used in template
               */
              if (type == 'config-one') {
                $scope.pushItem('configOne', group);
              }
              if (type == 'config-many') {
                $scope.pushItem('configMany', group);
              }

              var orderArray = angular.copy($scope.offers.orderArray);
              angular.forEach(group.items, function(item, key) {
                if (item.hasOwnProperty('selected') && (item.selected == true)) {
                  if ((item.autoRenew == true) || ( item.hasOwnProperty('future') && item.future.hasOwnProperty('intervalUnit') && item.future.intervalUnit == 'adhoc')) {
                    $scope.showAutoRenewMsg = true;
                  }
                  var index = orderArray.indexOf(group);
                  if (index == -1) {
                    orderArray.push(group);
                  }
                }

                if (!!item.groups && item.groups.length > 0) {
                  $scope.iterate(repeat, item.groups);
                } else {
                  repeat = false;
                }
              });
              $scope.offers.orderArray = angular.copy(orderArray);
            });
          };
          $scope.iterate(repeat, elem);

          var newArray = angular.copy($scope.offers.orderArray);
          for (var j = newArray.length - 1; j >= 0; j--) {
            for (var i = newArray[j].items.length - 1; i >= 0; i--) {
              try {
                if (($scope.offers.belowDriverList && $scope.offers.belowDriverList.indexOf(newArray[j].items[i].path) > -1) && !$scope.offers.belowdriver.hasOwnProperty(newArray[j].items[i].path)) {
                  newArray[newArray.indexOf(newArray[j])].items.splice(i, 1);
                }
              } catch (e) {
                debug('Foundation', e);
              }
            }

          }
          $scope.offers.orderArray = angular.copy(newArray);

        };


        $scope.customColor = function(color) {
          return {"background-color": color.valueOf()};
        };

        $scope.imageStyle = function(item) {
          if (item == null || item.image == null || item.image == undefined || item.image.length == 0) {
            return;
          }
          return {'background-image': 'url(' + item.image + ')'};
        };

        $scope.showBankPaymentWireInstructionDialog = function(instruction) {
          var modalInstance = $modal.open({
            templateUrl: 'bankPaymentWireInstructionDialog.html',
            windowClass: 'dialog-medium',
            controller: function($scope, $modalInstance) {
              $scope.instruction = instruction;
              $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
              };
            }
          });
        };

        $scope.refreshPageResponse = function(response) {
          env.phrases = response.phrases;
          $scope.variables = response.variables;
          $scope.properties = response.properties;
          $scope.pending = response.pending;
          $scope.messages = response.messages;
          $scope.orderReference = response.orderReference;
          $scope.session = response.session;
          $scope.country = response.order.country;
          $scope.coupon = response.order.coupons[0] || '';
          $scope.totalSavings = response.order.discountTotal;
          $scope.totalSavingsPercent = response.order.discountTotalPercent;
          $scope.totalSavingsValue = response.order.discountTotalValue;

          $scope.paymentOption = response.paymentOption;
          $scope.buyerEmail = response.buyerEmail;
          $scope.language = response.language;
          $scope.shipping = response.shipping;
          $scope.isAutoRenew = response.autoRenew;

          $scope.orderDate = response.order.orderDate;
          $scope.currentTime = response.order.currentTime;

          $rootScope.accountManagement = response.accountManagement;

          //sandbox browserhooks
          $scope.events = response.events || null;

          $scope.items = []; // complete currently has a flattened view of items
          angular.forEach(response.order.groups, function(topGroup) {
            angular.forEach(topGroup.items, function(topItem) {
              if (topItem.selected) {
                $scope.items.push(topItem);
                angular.forEach(topItem.groups, function(subGroup) {
                  if (subGroup.type != 'bundle') {
                    angular.forEach(subGroup.items, function(subItem) {
                      if (subItem.selected) {
                        $scope.items.push(subItem);
                      }
                    });
                  }
                });
              }
            });
          });

          $scope.groups = response.order.groups;

          $scope.fulfillment = response.fulfillment;
          $scope.bankPaymentWireInstruction = response.bankPaymentWireInstruction;
          $scope.purchaseOrderInstruction = response.purchaseOrderInstruction;
          $scope.total = response.order.total;
          $scope.tax = response.order.tax;
          $scope.totalValue = response.order.totalValue;
          $scope.taxValue = response.order.taxValue;
          $scope.totalWithTax = response.order.totalWithTax;
          $scope.billDescriptor = response.billDescriptor;
          $scope.orderReference = response.orderReference;
          $scope.order = response.order;
          $scope.taxExempt = response.order.taxExempt;
          $scope.taxRate = response.order.taxRate;
          $scope.taxType = response.order.taxType;
          $scope.taxPriceType = response.order.taxPriceType;
            
          $scope.currency = response.order.currency;

          $scope.invoiceLink = window.location.origin + '/account/order/' + $scope.orderReference + '/invoice';

          $timeout(function() {
            $scope.createArrays();

          }, 0);
        };

        $scope.wireTransferInvoiceLink = function(orderReference){
          return window.location.origin + '/account/order/' + orderReference + '/invoice';
        };

        $scope.defineMetadata = function() {

          window.theme = 'Foundation';
          window.live = $scope.session.live;

          try {
            if ($scope.session.primary && $scope.session.primary.length > 0) {
              window.vendor = $scope.session.primary;
            }
          } catch (e) {
            window.vendor = '';
          }

          try {
            if ($scope.session.secondary && $scope.session.secondary.length > 0) {
              window.storefront = $scope.session.secondary;
            }
          } catch (e) {
            window.storefront = '';
          }
        };

        function track(obj) {
          var data = JSON.parse(document.getElementById('viewdata').innerHTML);
          var timeAgo = new Date().getTime();
          var orderDate = data.orderDate;
          var tenMins = 10 * 60 * 1000;

          // Only track analytics for tenMins minutes after an order was created
          if (timeAgo - orderDate < tenMins) {
            analyze(obj);
          }
        }

        window.trackInitialPageView = function() {
          $scope.itemArr = [];
          $scope.items.forEach(function(item) {
            $scope.itemObj = {};
            var coupon = '';
            if (item.hasOwnProperty('discount') && item.discount.hasOwnProperty('coupon')) coupon = item.discount.coupon;

            $scope.itemObj = {
              'name': String(item.display),
              'id': String(item.path),
              'price': String(item.unitPriceValue),
              'quantity': String(item.quantity),
              'coupon': String(coupon)
            };
            $scope.itemArr.push($scope.itemObj);
          });

          $scope.purchaseObj = {
            'id': String($scope.orderReference),
            //'affiliation': 'FastSpring Online Store',
            'revenue': $scope.totalValue,
            'tax': $scope.taxValue,
            'shipping': '0',
            'coupon': $scope.coupon
          };

          var paymentMethod = '';
          if ($scope.paymentOption) {
            paymentMethod = $scope.paymentOption.type;
          } else if ($scope.purchaseOrderInstruction) {
            paymentMethod = 'purchaseorder';
          }

          try {
            track({
              'fsc-url': window.location.href,
              'fsc-referrer': document.referrer,
              'fsc-currency': String($scope.currency),
              'fsc-order-id': String($scope.orderReference),
              'fsc-order-tax': $scope.taxValue,
              'fsc-order-total': $scope.totalValue,
              'fsc-coupon': $scope.coupon,
              'fsc-paymentMethod': paymentMethod,
              'ecommerce': {
                'currencyCode': String($scope.currency),
                'purchase': {
                  'actionField': $scope.purchaseObj,
                  'products': $scope.itemArr
                }
              }
            });
          } catch (e) {
            logMessage({
              'primary': $scope.session.primary,
              'secondary': $scope.session.secondary,
              'action': 'Complete:Analyze:Pageview',
              'error': e.message
            });
            console.log(e);
          }

          try {
            track({
              'event': 'FSC-purchaseComplete',
              'fsc-url': window.location.href,
            'fsc-referrer': document.referrer,
              'fsc-currency': String($scope.currency),
              'fsc-eventCategory': 'Checkout',
              'fsc-eventAction': 'Purchase Complete',
              'fsc-eventLabel': 'Total Order Value',
              'fsc-eventValue': parseInt($scope.totalValue, 10),
              'fsc-paymentMethod': paymentMethod
          });
          } catch (e) {
            logMessage({
              'primary': $scope.session.primary,
              'secondary': $scope.session.secondary,
              'action': 'Complete:Analyze:Event',
              'error': e.message
            });
            console.log(e);
          }

          if ($scope.events !== null && $scope.events.length > 0) {

            track({
              '_browserhooks': $scope.events
            });

          }

        };


        // Init
        $scope.complianceData = {};
        $scope.refreshPageResponse(initData);
        angular.element(document.getElementById('app')).removeClass('updating').addClass('loaded');

        angular.element(document).ready(function() {
          $scope.defineMetadata();

          try {
            initSandbox($scope.session.sandbox + window.location.pathname);
          } catch (e) {
            logMessage({
              'primary': $scope.session.primary,
              'secondary': $scope.session.secondary,
              'action': 'Complete:initSandbox:Exception',
              'path': $scope.session.sandbox + window.location.pathname,
              'error': e.message
            });
            (console.error || console.log)(e);
          }

          logMessage({
            'primary': $scope.session.primary,
            'secondary': $scope.session.secondary,
            'action': 'Complete:RegisterPurchase',
            'orderId': $scope.orderReference
          });

          if (localStorage.hasOwnProperty('popupShown') && localStorage.getItem('popupShown')) {
            localStorage.removeItem('popupShown');
          }

          window.opener && window.opener.postMessage({paymentResult: 'success'}, '*');

          // Tracking analytics
          <!-- Aptrinsic Tag (Gainsight) -->
          (function(n,t,a,e){var i="aptrinsic";n[i]=n[i]||function(){
            (n[i].q=n[i].q||[]).push(arguments)},n[i].p=e;
            var r=t.createElement("script");r.async=!0,r.src=a+"?a="+e;
            var c=t.getElementsByTagName("script")[0];c.parentNode.insertBefore(r,c)
          })(window,document,"https://web-sdk.aptrinsic.com/api/aptrinsic.js", "AP-G80B2PFE4NVZ-2");
          //passing user and account objects:
          aptrinsic("identify",
              {
                //User Fields (logged in user)
                "id": $scope.session.primary + ':' + $scope.session.secondary, // Unique for each checkout completed
                "store": window.storefront,
                //Custom attributes - please create those custom attributes in Aptrinsic via Account Settings to be tracked.
                "live" : window.live,
                "referrer": document.referrer,
                "currency": String($scope.currency),
                "orderReference": String($scope.orderReference)
              },
              {
                //Seller/store info
                "id":window.vendor, //Seller/vendor
                "name":window.vendor
              });
          <!-- End Aptrinsic Tag -->

          <!-- Pendo Tag -->
          (function(apiKey){
            (function(p,e,n,d,o){var v,w,x,y,z;o=p[d]=p[d]||{};o._q=[];
              v=['initialize','identify','updateOptions','pageLoad'];for(w=0,x=v.length;w<x;++w)(function(m){
                o[m]=o[m]||function(){o._q[m===v[0]?'unshift':'push']([m].concat([].slice.call(arguments,0)));};})(v[w]);
              y=e.createElement(n);y.async=!0;y.src='https://cdn.pendo.io/agent/static/'+apiKey+'/pendo.js';
              z=e.getElementsByTagName(n)[0];z.parentNode.insertBefore(y,z);})(window,document,'script','pendo');

            // Call this whenever information about your visitors becomes available
            // Please use Strings, Numbers, or Bools for value types.
            pendo.initialize({
              visitor: {
                id: $scope.session.id, // Unique for each checkout completed
                store : window.storefront,
                live : window.live,
                referrer: document.referrer,
                currency: String($scope.currency),
                orderReference: String($scope.orderReference),
                email: $scope.buyerEmail,
                country : $scope.country,
                ipAddress: $scope.session.ipAddress,
                device : navigator.userAgent
              },
              account: {
                id: $scope.session.primary + '_' + $scope.session.secondary,
                name : $scope.session.secondary,
                companyId : $scope.session.companyId,
                storePath : window.vendor
              }
            });
          })('c5418cc8-d6c3-42dc-4618-009ab35637eb');
          <!-- End Pendo Tag -->
        });
      }]);
})();
